<?php
/**
 * class Registry
 */
class Registry
{
    /**
     * @var array
     */
    protected static $storage = array();

    /**
     * sets a value
     *
     * @param string $key
     * @param mixed $value
     *
     * @static
     * @return void
     */
    public static function set($key, $value)
    {
        self::$storage[$key] = $value;
    }

    /**
     * gets a value from the registry
     *
     * @param string $key
     *
     * @static
     * @return mixed
     */
    public static function get($key)
    {
        return self::$storage[$key];
    }
}