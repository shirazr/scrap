<?php
/**
 * Class Config
 *
 * <code>
 *
 * //Some where on the application bootstrap, load the config once
 * Config::loadFromFile('/path/to/config.php');
 * Config::loadFromFile('/path/to/services.php');
 *
 * //using the configuration in classes and other places
 * $dbConfig = Config::get('db');
 *
 * echo $dbConfig['host'];
 *
 * </code>
 */
class Config
{
    /**
     * @var array
     */
    protected static $storage = array();

    /**
     * sets a value
     *
     * @param string $key
     * @param mixed $value
     *
     * @static
     * @return void
     */
    public static function set($key, $value)
    {
        self::$storage[$key] = $value;
    }

    /**
     * gets a value from the registry
     *
     * @param string $key
     *
     * @static
     * @return mixed
     */
    public static function get($key)
    {
        return self::$storage[$key];
    }

    /**
     * Load From file
     *
     * @param $filename
     * @return array
     * @throws Exception
     */
    public static function loadFromFile($filename)
    {
        if(!file_exists($filename))
        {
            throw new Exception($filename . ' not found');
        }

        $config = include $filename;

        foreach($config as $key => $value)
        {
            self::set($key, $value);
        }
        return self::$storage;
    }
}